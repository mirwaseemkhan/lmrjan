﻿<?php
    include('../includes/header.php'); 
?>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="portfolios">
                <div class="text-center">
                    <h2>PPE (Personal Protective Equipment)</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    PhaseCore® personal cooling vests can be used in any extreme heat environment worn under the clothing which uses a 
                    revolutionary phase change material to absorb outside heat and keep the body at a comfortable temperature. It is unlike 
                    any other personal cooling technology with no threat of hypothermia. It is produced in variety of fabrics from porous to
                     FR, its uses are unlimited including:
                    <ul>
                        <li>Military & Fire Rescue Services</li>
                        <li>Hospitals (MS patients)</li>
                        <li>First Responders</li>
                        <li>Industry</li>
                        <li>Outdoor Wear, Sports</li>
                    </ul> 
                </p>
            </div>
        </div>
    </div>
</div>


<?php include('../includes/Footer.php') ?>


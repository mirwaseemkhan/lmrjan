﻿<?php
    include('../includes/header.php'); 
?>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="portfolios">
                <div class="text-center">
                    <h2>Surgical & Clinical Disposables</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    The world of patient care continues to change in terms of facilities as well the containment of communicable diseases as 
                    witnessed by increasing desire of both patients and health service providers. We have partnered with leading suppliers of 
                    non-woven fabric to complement our line of Surgical & Clinical Attire.<br /><br />
                    Manufactured by the best non-woven fabric specification/s of customer’s choice and in anti-microbial packing, products 
                    are ensured to be health compliant conforming to standards, we provide following in custom sizes along with OTC:
                    <ul>
                        <li>Gowns (Surgical, Examination, Impervious)</li>
                        <li>Drapes & Coverings (All types)</li>
                        <li>Medical Accessories (Incl. Patient Suits, Hospital Linen)</li>
                        <li>Clinical Attire & Disposables </li>
                    </ul>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container" id="opsread1">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="portfolios">
                <div class="text-center">
                    <h2>Safety, Management & Compliance Through Traceability</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../includes/Footer.php') ?>


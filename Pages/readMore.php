﻿<?php
    include('../includes/header.php'); 
?>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="portfolios">
                <div class="text-center">
                    <h2>PPE (Personal Protective Equipment)</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    PhaseCore® personal cooling vests can be used in any extreme heat environment worn under the clothing which uses a 
                    revolutionary phase change material to absorb outside heat and keep the body at a comfortable temperature. It is unlike 
                    any other personal cooling technology with no threat of hypothermia. It is produced in variety of fabrics from porous to
                     FR, its uses are unlimited including:
                    <ul>
                        <li>Military & Fire Rescue Services</li>
                        <li>Hospitals (MS patients)</li>
                        <li>First Responders</li>
                        <li>Industry</li>
                        <li>Outdoor Wear, Sports</li>
                    </ul> 
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="portfolios">
                <div class="text-center">
                    <h2>Emergency Medical Services</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    MedNex® provides medical response equipment that aids in medical triage before and during mass casualty transportation. 
                    MedNex® products work seamlessly with the AmbuResponse® products to provide top quality care during a mass casualty incident. 
                    The uses of MedNex® and AmbuResponse® are within the scope of:
                    <ul>
                        <li>Disaster Management & Relief Operations</li>
                        <li>Mass Casualty & Non-Ambulatory Movement</li>
                        <li>Flood Relief Operations & Rescue Services</li>
                    </ul> 
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="portfolios">
                <div class="text-center">
                    <h2>Surgical & Clinical Disposables</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    The world of patient care continues to change in terms of facilities as well the containment of communicable diseases as 
                    witnessed by increasing desire of both patients and health service providers. We have partnered with leading suppliers of 
                    non-woven fabric to complement our line of Surgical & Clinical Attire.<br /><br />
                    Manufactured by the best non-woven fabric specification/s of customer’s choice and in anti-microbial packing, products 
                    are ensured to be health compliant conforming to standards, we provide following in custom sizes along with OTC:
                    <ul>
                        <li>Gowns (Surgical, Examination, Impervious)</li>
                        <li>Drapes & Coverings (All types)</li>
                        <li>Medical Accessories (Incl. Patient Suits, Hospital Linen)</li>
                        <li>Clinical Attire & Disposables </li>
                    </ul>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container" id="opsread1">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="portfolios">
                <div class="text-center">
                    <h2>Safety, Management & Compliance Through Traceability</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio" id="opsread2">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    OpsSmart® is an established software company focused on safety assurance and traceability solutions for the food supply
                    chain across all international locations. OpsSmart® provides the ability to trace raw materials through the supply chain
                    to the final sale, providing the customer transparency in the supply chain and increasing quality and safety. OpsSmart®
                    caters to:
                    <ul>
                        <li>Food Sector (From Farm to Fork)</li>
                        <li>Government (Regulatory Bodies)</li>
                        <li>Supply Cain (Wholesale Distribution to Retail)</li>
                    </ul>
                </p>
            </div>
        </div>
    </div>
</div>

<?php include('../includes/Footer.php') ?>


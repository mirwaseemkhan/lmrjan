﻿<?php
    include('../includes/header.php');
?>
    <div class="container signIn-container">
    	<div class="row">
            
            <div class="col-md-6 col-md-offset-3">
                <img class="mainImage" src="/img/Logo3_LMRJAN-PK.png"/>
            </div>
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary myBody">
                    <form action="../Actions/doLogin.php" method="post">
                        <div class="Mypanel">
                            SIGN IN
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="username">User Name:</label>
                                <input type="text" class="form-control" id="meail" name="email">
                            </div>
                            <div class="form-group">
                                <label for="password">Password:</label>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                            <div class="form-group">
                                <input class="btn signIn-button" value="Sign In" type="submit" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
	 <!--Footer Ends-->
<?php
    include('../includes/Footer.php');
?>

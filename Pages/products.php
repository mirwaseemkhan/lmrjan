﻿<?php
	include('../includes/header.php'); 
?>	
	<div class="container">
		<div class="row">
			<div class="service">
				<div class="col-md-6 col-md-offset-3">
					<div class="text-center text-justify">
						<h2>Products</h2>
                        <p style="color:black;">
                            We have formed alliances with the world’s premier vendors in the fields of PPE & Emergency Response Equipment
                            manufacturer and Compliance & Traceability Systems provider using advanced information technology tools.
                            Leveraging these partnerships, the services we offer our clients fall under four broad categories <br>
                        </p>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="services">
		<div class="container">
		<h2>Categories</h2><br \>
			<div class="row"  >
				<div class="col-md-3">
					<div class="wow bounceIn" data-wow-offset="0" data-wow-delay="0.4s">
						<h4>PPE (Personal Protective Equipment)</h4>					
							<div class="icon">
								<!-- <i class="fa fa-heart-o fa-3x"></i> -->
							</div>						
						<p>
                        PhaseCore® personal cooling vests can be used in any extreme heat environment worn under the clothing which uses a...
                        </p>
						<div class="ficon">
                            <a href="ppe.php" class="btn btn-primary" role="button">Read more</a>
						</div>
					</div>
				</div>
				
				<div class="col-md-3">
					<div class="wow bounceIn" data-wow-offset="0" data-wow-delay="1.0s">
						<h4>EMS (Emergency Medical Services)</h4>
						<div class="icon">
							<!-- <i class="fa fa-desktop fa-3x"></i> -->
						</div>
						<p>
                            MedNex® provides medical response equipment that aids in medical triage before and during mass casualty transportation. 
                        </p>
						<div class="ficon">
                            <a href="ems.php" class="btn btn-primary" role="button">Read more</a>
						</div>
					</div>
				</div>

                <div class="col-md-3">
                    <div class="wow bounceIn" data-wow-offset="0" data-wow-delay="1.6s">
                        <h4>Surgical & Clinical Disposables</h4>
                        <div class="icon">
                            <!-- <i class="fa fa-laptop fa-3x"></i> -->
                        </div>
                        <p>
                            The world of patient care continues to change in terms of facilities as well the containment of communicable
                            diseases as witnessed by increasing desire of both patients and...
                        </p>
                        <div class="ficon">
                            <a href="surigcal&ClinicalDisposables.php" class="btn btn-primary" role="button">Read more</a>
                        </div>
                    </div>
                </div>

				<div class="col-md-3" id="ops">
					<div class="wow bounceIn" data-wow-offset="0" data-wow-delay="2.2s">
						<h4>Food Safety & Traceability</h4>
						<div class="icon">
							<!-- <i class="fa fa-location-arrow fa-3x" ></i> -->
						</div>
						<p>OpsSmart® is an established software company focused on safety assurance and traceability solutions for the food supply chain across all international locations. </p>
						<div class="ficon">
                            <a href="foodSafety.php" class="btn btn-primary" role="button">Read more</a>
						</div>
					</div>					
				</div>
				
				
			</div>
		</div>
	</div>
	
<?php include('../includes/Footer.php') ?>


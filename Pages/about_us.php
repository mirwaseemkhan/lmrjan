﻿<?php
    include('../includes/header.php'); 
?>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="portfolios">
                <div class="text-center">
                    <h2>About Us</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    LMRJAN-PK is a Business Development Group working towards providing business access services to product owners and markets alike. We are focused in delivering timely solutions best suited to our customers.<br /><br />
                    Customer satisfaction is important to our success and we operate as a trusted partner to each of our Clients. We dedicate necessary highly skilled resource to meet business development project requirements from its initial study all through the entire project life-cycle.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="portfolios">
                <div class="text-center">
                    <h2 id="partners">Our Partners</h2><br />
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>We have always maintained high quality services and reinforced its market position by means of strategic alliances with international business partners as well as local. </p><br />
                <table id="t1" class="table-bordered" style="width:100%">
                    <tr>
                        <td>
                            <a href="https://www.firstlinetech.com/" target="_blank">
                                <img src="~/img/FLT.jpg" alt="First Line Technologies LLC." style="width:150px;height:70px;padding:10px;">
                            </a>
                        </td>
                        <td style="padding:10px;">
                            First Line Technologies LLC. has positioned itself to be a leader in the field PPE, Medical Response & Hazzard Preparedness using innovative solutions combined with latest technology.<br />
                            We are partnered with First Line Technologies Inc. for PhaseCore® Vest fabrication needs outside of The United States of America. At LMRJAN-PK we are actively assisting First Line Technologies LLC.’s Business Development endeavors in Pakistan.<br />
                        </td>
                    </tr>
                    <tr id="ops">
                        <td>
                            <a href="https://www.opssmartglobal.com/" target="_blank">
                                <img src="~/img/opsSmart.png" alt="OpsSmart Technologies Inc." style="width:150px;height:70px;padding:10px;">
                            </a>
                        </td>
                        <td style="padding:10px;">
                            The OpsSmart® Technologies, Inc is one of the leading providers of food traceability solution. OpsSmart® traceability software has been utilized by small farms, large agribusinesses, retailers and governmental agencies for over 10 years.
                            In February 2014, OpsSmart® Technologies, Inc.<br />(OpsSmart), took over the operations of FXA group, Limited, a preeminent provider of traceability solutions. OpsSmart® is in its third generation; with major food suppliers in all major food
                            categories, including but not limited to seafood, poultry, swine, produce, rice, meat and pharmaceuticals. OpsSmart has offices in the USA and Thailand.<br />
                            OpsSmart® is focused on developing and deploying safety assurance and traceability solutions for the supply chain across international locations. OpsSmart® provides the ability to trace raw materials through the supply chain to the final sale,
                            providing its customers transparency in the supply chain while assuring quality and security. In doing so, OpsSmart® efficiently mitigates the growing security, traceability and safety concerns.
                        </td>
                    </tr>

                </table>
            </div>

        </div>
    </div>
</div>

<div class="portfolio-2">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                
            </div>
        </div>
    </div>
</div>
<?php include('../includes/footer.php') ?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/jquery-2.1.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/wow.min.js"></script>
<script src="/js/jquery.easing.1.3.js"></script>
<script src="/js/jquery.isotope.min.js"></script>
<script src="/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/js/fliplightbox.min.js"></script>
<script src="/js/functions.js"></script>
<script type="text/javascript">$('.portfolio').flipLightBox()</script>

<!--my script-->
<script>
    $(document).ready(function () {
        $("#ops").hide();
    });
</script>
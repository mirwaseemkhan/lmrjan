﻿<?php
    include('../includes/header.php'); 
?>	
	<div class="map">
		<iframe src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Kuningan,+Jakarta+Capital+Region,+Indonesia&amp;aq=3&amp;oq=kuningan+&amp;sll=37.0625,-95.677068&amp;sspn=37.410045,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=Kuningan&amp;t=m&amp;z=14&amp;ll=-6.238824,106.830177&amp;output=embed">
		</iframe>
	</div>
	
	<section id="contact-page">
        <div class="container">
            <div class="center">        
                <h2>Drop Your Message</h2>
                
            </div> 
            <div class="row contact-wrap"> 
                <div class="status alert alert-success" style="display: none"></div>
                <form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="~/Action/SendEmail">
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="form-group">
                            <label>Name *</label>
                            <input type="text" name="name" placeholder="e.g. Muhammad Ali" class="form-control" required="required" autofocus>
                        </div>
                        <div class="form-group">
                            <label>Email *</label>
                            <input type="email" name="receiver" placeholder="e.g. abc@xyz.com" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Phone *</label>
                            <input type="tel" pattern="^\d{20}" maxlength="20" placeholder="e.g. 03217897897" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Company Name</label>
                            <input type="text" name="companyName" placeholder="e.g. ABC Enterprises" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Subject *</label>
                            <input type="text" name="subject" placeholder="Your subject here" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Message *</label>
                            <textarea name="message" id="message" placeholder="Your message here" required="required" class="form-control" rows="8"></textarea>
                        </div>
                        <div class="form-group">
                            <button id="submitMessage" type="submit" name="submit" class="btn btn-primary btn-lg" onclick="submitMessage()">Submit Message</button>
                        </div>
                    </div>
                </form> 
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#contact-page-->
	
<?php include('../includes/Footer.php') ?>
	
   
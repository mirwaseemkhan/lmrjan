﻿<?php
    include('../includes/header.php'); 
?>

<div class="container" id="opsread1">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="portfolios">
                <div class="text-center">
                    <h2>Food Safety & Traceability</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio" id="opsread2">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    OpsSmart® is an established software company focused on safety assurance and traceability solutions for the food supply
                    chain across all international locations. OpsSmart® provides the ability to trace raw materials through the supply chain
                    to the final sale, providing the customer transparency in the supply chain and increasing quality and safety. OpsSmart®
                    caters to:
                    <ul>
                        <li>Food Sector (From Farm to Fork)</li>
                        <li>Government (Regulatory Bodies)</li>
                        <li>Supply Cain (Wholesale Distribution to Retail)</li>
                    </ul>
                </p>
            </div>
        </div>
    </div>
</div>

<?php include('../includes/Footer.php') ?>


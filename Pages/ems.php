﻿<?php
    include('../includes/header.php'); 
?>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="portfolios">
                <div class="text-center">
                    <h2>Emergency Medical Services</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    MedNex® provides medical response equipment that aids in medical triage before and during mass casualty transportation. 
                    MedNex® products work seamlessly with the AmbuResponse® products to provide top quality care during a mass casualty incident. 
                    The uses of MedNex® and AmbuResponse® are within the scope of:
                    <ul>
                        <li>Disaster Management & Relief Operations</li>
                        <li>Mass Casualty & Non-Ambulatory Movement</li>
                        <li>Flood Relief Operations & Rescue Services</li>
                    </ul> 
                </p>
            </div>
        </div>
    </div>
</div>

<?php include('../includes/Footer.php') ?>


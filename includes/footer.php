<!--Footer Begins-->
    <footer>

        <div class="last-div">
            <div class="container">
                <div class="row">
                   
                    <div class="col-md-2 f-contact" >
                        <h3 class="widgetheading">Stay in touch</h3>
                        <p><i class="fa fa-envelope"></i> info@lmrjan.com</p>
                        <p><i class="fa fa-phone"></i> +92 300 838 1339</p>
                    </div>
                    <div class="col-md-6 copyright">
                        &copy; LMRJAN-PK | <a href="privacyPolicy.php" target="_blank">Privacy Policy</a>
                    </div>
                </div>
            </div>
            <a href="" class="scrollup"><i class="fa fa-chevron-up"></i></a>
        </div>
    </footer>
    <!--Footer Ends-->
    <script src="/Scripts/jquery-2.1.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/Scripts/bootstrap.min.js"></script>
    <script src="/Scripts/wow.min.js"></script>
    <script src="/Scripts/jquery.easing.1.3.js"></script>
    <script src="/Scripts/jquery.isotope.min.js"></script>
    <script src="/Scripts/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="/Scripts/fliplightbox.min.js"></script>
    <script src="/Scripts/functions.js"></script>
    <script type="text/javascript">$('.portfolio').flipLightBox()</script>
</body>
<!--Body Ends-->
</html>

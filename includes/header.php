<?php 
    session_start();
    // $row = null;
    // if(isset($_SESSION['user']))
    // {
    //     $row = $_SESSION['user'];
    // }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LMRJAN-PK</title>
    <link rel="icon" 
      type="image/png" 
      href="../img/Logo3_LMRJAN-PK.png">
    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">        
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="/css/set1.css" />
    <link href="/css/overwrite.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/myStyles.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--Body Begins-->
<body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse.collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img class="navbar-left myLogo" src="../img/Logo3_LMRJAN-PK.png" alt="lmrjan logo"/>
                <a class="navbar-brand" href="../index.php"><span>LMRJAN-PK</span></a>
                
            </div>
            <div class="navbar-collapse collapse">
                <div class="menu">
                    <ul class="nav nav-tabs my-nav" role="tablist">
                        <li role="presentation" ><a href="../index.php">Home</a></li>
                        <li role="presentation" ><a href="../Pages/products.php">Products</a></li>
                        <li role="presentation" ><a href="../Pages/resources.php">Resources</a></li>s
                        <li role="presentation" ><a href="../Pages/about_us.php">Services</a></li>
                        <li role="presentation" ><a href="../Pages/contact.php">Contact</a></li>
                        <?php                        
                            if(isset($_SESSION["user"])){
                                if($_SESSION["user"] == 'admin'){
                                    echo('<li role="presentation"><a href="../Pages/projects.php">Projects</a></li> 
                                    <li role="presentation"><a href="../Actions/doLogout.php">Log Out</a></li> ');
                                }else{
                                    echo('<li role="presentation"><a href="../Pages/signIn.php">Projects</a></li>');
                                }
                                
                            }else{
                                echo('<li role="presentation"><a href="../Pages/signIn.php">Projects</a></li>');
                            }
                        ?>                                                
                    </ul>
                </div>
            </div>
        </div>
    </nav>